<?php

namespace DrupalCoreSplit\Command;

use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PurgeCommand extends CommandBase {

  protected static $defaultName = 'purge';

  protected function configure(): void {
    // @todo Add description.
    $this
      ->addRefArgument(InputArgument::REQUIRED)
      ->addSha1Argument(InputArgument::REQUIRED)
      ->addReftypeArgument(InputArgument::REQUIRED)
      ->addConfigOption();
  }

  protected function execute(InputInterface $input, OutputInterface $output): int {
    try {
      $this->cleanupWorkDir();
    }
    catch (RuntimeException $e) {
      $this->io->error($e->getMessage());
      return Command::FAILURE;
    }
    return Command::SUCCESS;
  }

  private function cleanupWorkDir() {
    if (file_exists($this->getParentWorkingDir())) {
      $this->io->section("Cleaning up {$this->getParentWorkingDir()}");
      $this->shell->passthru("rm -rf {$this->getParentWorkingDir()}");
    }
  }

}
