<?php

namespace DrupalCoreSplit\Command;

use RuntimeException;
use DrupalCoreSplit\Utility\ConfigHandler;
use DrupalCoreSplit\Utility\ShellFacade;
use DrupalCoreSplit\Utility\SubtreeHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class CommandBase extends Command {

  /** @var InputInterface */
  protected $args;
  /** @var ConfigHandler */
  protected $config;
  /** @var SymfonyStyle */
  protected $io;
  /** @var string|null */
  protected $ref;
  /** @var string|null */
  protected $reftype;
  /** @var string|null */
  protected $sha1;
  /** @var ShellFacade */
  protected $shell;
  /** @var SubtreeHandler */
  protected $subtrees;
  protected $templates = ['recommended-project', 'legacy-project'];

  protected function initialize(InputInterface $input, OutputInterface $output) {
    $this->args = $input;
    $this->io = new SymfonyStyle($input, $output);
    $this->config = new ConfigHandler();
    $filename = $input->getOption('config');
    if ($filename) {
      $this->config->setConfigOverrideFile($filename);
    }
    $this->ref = $this->setConditionalArgumentValue('ref');
    $this->reftype = $this->setConditionalArgumentValue('reftype');
    $this->sha1 = $this->setConditionalArgumentValue('sha1');
    $this->shell = new ShellFacade();
    $this->subtrees = new SubtreeHandler();
  }

  private function setConditionalArgumentValue(string $name): ?string {
    return $this->args->hasArgument($name)
      ? $this->args->getArgument($name)
      : NULL;
  }

  protected function addRefArgument(int $mode): CommandBase {
    $this->assertIsInputArgumentMode($mode);
    // @todo Add description.
    return $this->addArgument('ref', $mode);
  }

  protected function addSha1Argument(int $mode): CommandBase {
    $this->assertIsInputArgumentMode($mode);
    // @todo Add description.
    return $this->addArgument('sha1', $mode);
  }

  protected function addReftypeArgument(int $mode): CommandBase {
    $this->assertIsInputArgumentMode($mode);
    // @todo Add description.
    return $this->addArgument('reftype', $mode);
  }

  protected function assertIsInputArgumentMode(int $mode): void {
    assert(in_array($mode, [
      InputArgument::REQUIRED,
      InputArgument::OPTIONAL,
      InputArgument::IS_ARRAY,
    ]));
  }

  protected function addConfigOption(): CommandBase {
    return $this->addOption(
      'config',
      NULL,
      InputOption::VALUE_REQUIRED,
      'A path to a configuration file. See config/config.yml.dist for the schema'
    );
  }

  protected function getParentWorkingDir(): string {
    return "{$this->config->getWorkingDirectory()}/{$this->config->getProjectToSplit()}/{$this->ref}/{$this->sha1}";
  }

  protected function getGeneratedSubtreeDir(string $name): string {
    return "{$this->getParentWorkingDir()}/generated-subtrees/{$name}";
  }

  protected function getSubtreeCheckoutDir(string $name): string {
    return "{$this->getParentWorkingDir()}/subtree-workdirs/{$name}";
  }

  protected function getProjectSourceDir(): string {
    return "{$this->getParentWorkingDir()}/source-project";
  }

  protected function getProjectVaultDir(): string {
    return "{$this->config->getWorkingDirectory()}/subtree-vaults/{$this->config->getProjectToSplit()}";
  }

  protected function getProjectVaultSubtreeDir(string $name): string {
    return "{$this->getProjectVaultDir()}/{$name}";
  }

  protected function setUpSourceRepository() {
    if (!file_exists($this->getProjectSourceDir())) {
      $this->shell->passthru("git -c advice.detachedHead=false clone -q {$this->config->getProjectUpstream()} {$this->getProjectSourceDir()}");
    }
    $this->shell->passthru("cd {$this->getProjectSourceDir()} && git remote set-url origin {$this->config->getProjectUpstream()} && git fetch origin && git fetch -t origin && git -c advice.detachedHead=false checkout {$this->ref}");
  }

  /**
   * @param string $delta_sha
   *   This is the sha1 of the commit that we wish to compare our ref against.
   *
   * @return array
   */
  protected function getChangedSubtrees(string $delta_sha): array {
    $subtrees = $this->subtrees->getAll($this->getProjectSourceDir());
    $files_changed_since_last_commit = $this->shell->exec("git -C {$this->getProjectSourceDir()} log --name-only --oneline {$delta_sha}.. --format= {$this->ref}");
    $all_files = json_encode($files_changed_since_last_commit);
    foreach ($subtrees as $subtree_name => $subtree_data) {

      // If the subtree path doesn't show up in the list of all paths, we'll
      // skip processing it.
      $encoded_subtree_path = str_replace('"', '', json_encode($subtree_data['path']));
      if (!strpos($all_files, $encoded_subtree_path)) {
        unset($subtrees[$subtree_name]);
      }

    }
    return $subtrees;
  }

  public function getChangedVaultRepos($delta_sha): array {
    $vault_repos = $this->subtrees->getVaultRepos($this->getProjectVaultDir());
    $changed_subtrees = $this->getChangedSubtrees($delta_sha);
    $changed_repos = [];
    foreach ($vault_repos as $repo) {
      if (isset($changed_subtrees[$repo])) {
        $changed_repos[] = $repo;
      }
    }
    if ($this->reftype == 'branch') {
      foreach ($this->templates as $template) {
        if (!in_array($template, $changed_repos)) {
          $changed_repos[] = $template;
        }
      }
    }

    return $changed_repos;
  }

  /**
   * Get the "core branch" of a version.
   *
   * @param string $version
   *   The version number to process.
   *
   * @return string
   *   The version number with everything after the last '.' stripped.
   */
  public function getCoreBranch(string $version): string {
    // Try branch like 11.1.x.
    $branch = preg_replace('#\.[^.]*$#', '.', $version) . 'x';
    if ($this->branchExists('origin/' . $branch)) {
      return $branch;
    }

    // Try branch like 11.x.
    $branch = preg_replace('#\.[^.]*\.[^.]*$#', '.', $version) . 'x';
    if ($this->branchExists('origin/' . $branch)) {
      return $branch;
    }

    // Try main branch.
    $branch = 'main';
    if ($this->branchExists('origin/' . $branch)) {
      return $branch;
    }

    throw new RuntimeException('Core branch could not be determined.');
  }

  private function branchExists(string $branch): bool {
    try {
      $this->shell->exec('git -C ' . escapeshellarg($this->getProjectSourceDir()) . ' show ' . escapeshellarg($branch));
    }
    catch (RuntimeException $e) {
      return FALSE;
    }

    return TRUE;
  }

}
