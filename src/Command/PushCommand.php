<?php

namespace DrupalCoreSplit\Command;

use DrupalCoreSplit\Utility\GitHubApi;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use RuntimeException;
use GuzzleHttp\Client as GuzzleClient;
use Packagist\Api\Client as PackagistClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PushCommand extends CommandBase {

  protected static $defaultName = 'push';

  protected function configure(): void {
    // @todo Add description.
    $this
      ->addRefArgument(InputArgument::OPTIONAL)
      ->addSha1Argument(InputArgument::OPTIONAL)
      ->addReftypeArgument(InputArgument::OPTIONAL)
      ->addConfigOption()
      ->addOption('no-packagist', NULL, InputOption::VALUE_NONE)
      ->addOption('push-all', NULL, InputOption::VALUE_NONE);
  }

  protected function execute(InputInterface $input, OutputInterface $output): int {
    try {
      $this->setUpSourceRepository();
      $this->pushSubtrees();
    }
    catch (RuntimeException $e) {
      $this->io->error($e->getMessage());
      return Command::FAILURE;
    }
    return Command::SUCCESS;
  }

  private function pushSubtrees() {
    if ($this->reftype === 'branch' && !$this->args->getOption('push-all')) {
      $vault_repos = $this->getChangedVaultRepos($this->sha1);
    }
    else {
      $vault_repos = $this->subtrees->getVaultRepos($this->getProjectVaultDir());
    }

    $github = new GitHubApi($this->config);
    $github_repos = $github->getRepos();
    foreach ($vault_repos as $repo_name) {
      try {
        $this->shell->exec("git -C {$this->getProjectVaultSubtreeDir($repo_name)} rev-parse {$this->ref} >/dev/null 2>&1");
      }
      catch (Exception $e) {
        $this->io->error($e->getMessage());
        continue;
      }
      $this->io->section("Pushing {$repo_name}");

      // If there's no GitHub repository for the subtree, create one.
      if (!in_array($repo_name, $github_repos, TRUE)) {
        $composer_json = $this->shell->exec("git -C {$this->getProjectVaultSubtreeDir($repo_name)} show {$this->ref}:composer.json");
        $file_contents = implode('', $composer_json);
        $json = json_decode($file_contents);
        $this->io->writeln("Creating {$repo_name} on GitHub");
        $github->createRepo($repo_name, $json->description);
      }

      if ($this->reftype == 'branch') {
        $this->io->writeln("Update branch {$this->ref}");
        $this->pushBranch($repo_name);
      }
      else {
        $this->io->writeln("Update tag {$this->ref}");
        $this->pushTag($repo_name);
      }

      if ($this->args->getOption('no-packagist')) {
        continue;
      }
      else {
        $url = $this->config->getGithubRepoUrlGit($repo_name);
        $this->updatePackagist($repo_name, $url, TRUE);
      }

    }
  }

  /**
   * Send create or update package API request to Packagist.org
   *
   * @param string $subtree_name
   *   The package name, “core-recommended” for “drupal/core-recommended”
   * @param string $url
   *   The repository URL.
   * @param bool $skip_update
   *   TRUE to skip sending update API requests to Packagist.org, which do not need to be sent when GitHub webhooks are configured for the repository.
   *
   * @return void
   */
  protected function updatePackagist(string $subtree_name, string $url, bool $skip_update = FALSE) {
    $packagist_client = new PackagistClient();

    // Check if package exists:
    $package_name = "drupal/{$subtree_name}";
    $body = json_encode([
      'repository' => [
        'url' => $url,
      ],
    ]);
    try {
      $packagist_client->get($package_name);
      $endpoint = 'update-package';
      if ($skip_update) {
        $this->io->writeln('Skipping updating at Packagist for ' . $package_name);
        return;
      }
    }
    catch (Exception $e) {
      // Package doesn't exist at Packagist: needs to be created.
      $endpoint = 'create-package';
    }

    $packagist_api_client = new GuzzleClient(['base_uri' => 'https://packagist.org/api/']);

    try {
      $this->io->writeln('Sending ' . $endpoint . ' to Packagist for ' . $package_name);
      $packagist_api_client->request('POST', $endpoint, [
        'body' => $body,
        'headers' => ['Content-Type' => 'application/json'],
        'query' => [
          'username' => $this->config->getGithubUsername(),
          'apiToken' => $this->config->getPackagistApiToken(),
        ],
      ]);
    }
    catch (GuzzleException $e) {
      throw new RuntimeException(sprintf('Failed to push to Packagist: %s', $e->getMessage()));
    }

  }

  protected function pushBranch($name = 'core') {
    $this->shell->passthru("git -C {$this->getProjectVaultSubtreeDir($name)} push --force {$this->config->getGithubRepoUrlHttp($name)} {$this->ref}");
  }

  protected function pushTag($name = 'core') {
    try {
      // If the tag is *not* there, the command fails, and throws an exception
      // Which tells us we need to create the tag.
      $this->shell->passthru("git ls-remote --exit-code --tags {$this->config->getGithubRepoUrlHttp($name)} {$this->ref}");
    }
    catch (RuntimeException $e) {
      $this->shell->passthru("git -C {$this->getProjectVaultSubtreeDir($name)} push {$this->config->getGithubRepoUrlHttp($name)} {$this->ref}", FALSE);
    }
  }

}
