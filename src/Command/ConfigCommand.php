<?php

namespace DrupalCoreSplit\Command;

use DrupalCoreSplit\Utility\ConfigHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConfigCommand extends CommandBase {

  protected static $defaultName = 'config';

  protected function configure(): void {
    $this
      ->setDescription('Display the active configuration')
      ->addConfigOption();
  }

  protected function execute(InputInterface $input, OutputInterface $output): int {
    $config = new ConfigHandler();
    $filename = $input->getOption('config');
    if ($filename) {
      $config->setConfigOverrideFile($filename);
    }

    $table = new Table($output);
    $table->setHeaders(['Key', 'Value']);
    foreach ($config->getConfig() as $key => $value) {
      $table->addRow([$key, $value]);
    }
    $table->render();

    return Command::SUCCESS;
  }

}
