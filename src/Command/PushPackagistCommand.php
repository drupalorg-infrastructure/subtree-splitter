<?php

namespace DrupalCoreSplit\Command;

use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PushPackagistCommand extends PushCommand {

  protected static $defaultName = 'push-packagist';

  protected function configure(): void {
    // @todo Add description.
    $this
      ->addArgument('project-name', InputArgument::REQUIRED)
      ->addRefArgument(InputArgument::OPTIONAL)
      ->addSha1Argument(InputArgument::OPTIONAL)
      ->addReftypeArgument(InputArgument::OPTIONAL)
      ->addConfigOption()
      ->addOption('no-packagist', NULL, InputOption::VALUE_NONE)
      ->addOption('push-all', NULL, InputOption::VALUE_NONE);
  }

  protected function execute(InputInterface $input, OutputInterface $output): int {
    try {
      $project_name = $this->args->getArgument('project-name');
      $url = "https://git.drupalcode.org/project/{$project_name}.git";
      $this->updatePackagist($project_name, $url);
    }
    catch (RuntimeException $e) {
      $this->io->error($e->getMessage());
      return Command::FAILURE;
    }
    return Command::SUCCESS;
  }

}
