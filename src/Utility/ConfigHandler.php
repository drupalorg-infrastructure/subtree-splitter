<?php

namespace DrupalCoreSplit\Utility;

use RuntimeException;
use Symfony\Component\Yaml\Yaml;

class ConfigHandler {

  private const SCHEMA = [
    'github_api_token' => 'string',
    'github_org_name' => 'string',
    'github_team_id' => 'integer',
    'github_username' => 'string',
    'packagist_api_token' => 'string',
    'project_to_split' => 'string',
    'project_upstream' => 'string',
    'working_directory' => 'string',
    'remap_org' => 'boolean',
    'php_path' => 'string',
  ];

  private $config;
  private $overrides = 'config/config.yml';
  private $yaml;

  public function __construct() {
    $this->yaml = new Yaml();
    $this->config = $this->getConfig();
  }

  public function setConfigOverrideFile(string $path) {
    if (!file_exists($path)) {
      throw new RuntimeException(sprintf('No such file %s', realpath($path)));
    }
    $this->overrides = $path;
    $this->config = $this->getConfig();
  }

  public function getConfig(): array {
    $config = array_merge(
      $this->getConfigDefaults(),
      $this->getConfigOverrides(),
      $this->getConfigSecrets()
    );
    $this->assertValidConfig($config);
    ksort($config);
    return $config;
  }

  public function getDrupalOrgProjectUrl(): string {
    return "https://www.drupal.org/project/{$this->getProjectToSplit()}";
  }

  public function getGithubRepoUrlGit(string $repo_name): string {
    return sprintf(
      'git@github.com:%s/%s.git',
      $this->getGithubOrgName(),
      $repo_name
    );
  }

  public function getGithubRepoUrlHttp(string $repo_name): string {
    return sprintf(
      'https://%s:%s@github.com/%s/%s.git',
      $this->getGithubUsername(),
      $this->getGithubApiToken(),
      $this->getGithubOrgName(),
      $repo_name
    );
  }

  private function assertValidConfig(array $config) {
    foreach (self::SCHEMA as $key => $type) {
      if (!array_key_exists($key, $config)) {
        throw new RuntimeException("Missing required '{$key}' config value.");
      }
      $value = $config[$key];
      if (gettype($value) !== $type) {
        throw new RuntimeException("Config value '{$key}' must be of type {$type}.");
      }
    }
  }

  private function getConfigDefaults(): array {
    return $this->getConfigFileData('config/config.yml.dist');
  }

  private function getConfigOverrides(): array {
    try {
      return $this->getConfigFileData($this->overrides);
    }
    catch (RuntimeException $e) {
      return [];
    }
  }

  private function getConfigSecrets(): array {
    return $this->getConfigFileData('config/config_secrets.yml');
  }

  private function getConfigFileData(string $path): array {
    if (!file_exists($path)) {
      return [];
    }

    $contents = file_get_contents($path);

    try {
      $data = $this->yaml::parse($contents);
    }
    catch (RuntimeException $e) {
      $data = FALSE;
    }

    if (!is_array($data) || empty($data)) {
      throw new RuntimeException(sprintf('Invalid config file contents in %s', realpath($path)));
    }

    return $data;
  }

  public function getGithubApiToken(): string {
    return $this->getConfigValue('github_api_token');
  }

  public function getGithubOrgName(): string {
    return $this->getConfigValue('github_org_name');
  }

  public function getGithubTeamId(): int {
    return $this->getConfigValue('github_team_id');
  }

  public function getGithubUsername(): string {
    return $this->getConfigValue('github_username');
  }

  public function getPackagistApiToken(): string {
    return $this->getConfigValue('packagist_api_token');
  }

  public function getProjectToSplit(): string {
    return $this->getConfigValue('project_to_split');
  }

  public function getProjectUpstream(): string {
    return $this->getConfigValue('project_upstream');
  }

  public function getWorkingDirectory(): string {
    return $this->getConfigValue('working_directory');
  }

  public function getRemapOrg(): bool {
    return $this->getConfigValue('remap_org');
  }

  public function getPhpPath(): string {
    return $this->getConfigValue('php_path');
  }

  private function getConfigValue(string $key): string {
    return $this->config[$key];
  }

}
