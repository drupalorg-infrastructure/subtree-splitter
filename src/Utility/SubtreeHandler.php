<?php

namespace DrupalCoreSplit\Utility;

use Symfony\Component\Finder\Finder;

class SubtreeHandler {

  /**
   * Gets all subtrees.
   *
   * Recursively scans the given directory for composer.json files. Any
   * directory that contains a composer.json is a split candidate.
   *
   * @param string $directory
   *   The directory of the repo we want to scan.
   *
   * @return array
   *   Returns an array of paths, indexed by their composer project name.
   */
  public function getAll(string $directory): array {
    $subtrees = [];

    $finder = new Finder();
    $finder->files()
      ->name('/composer.json$/')
      ->in($directory)
      ->exclude('tests');
    foreach ($finder as $file) {
      // Skip root composer.json.
      if ($file->getRelativePath() === '') {
        continue;
      }
      $json = json_decode($file->getContents());
      $project_name = str_replace('drupal/', '', $json->name);
      if (strpos($project_name, '/') !== FALSE) {
        // Skip any potential subtree with a name in composer.json that still
        // contains '/' after removing the 'drupal/' prefix. Includes:
        // - Libraries brought into D7, like
        // https://git.drupalcode.org/project/drupal/-/blob/aea09872a3db16ee4606865d0cf52f45b90d1a5e/misc/brumann/polyfill-unserialize/composer.json#L2
        // - Test fixtures in current Drupal, like
        // https://git.drupalcode.org/project/drupal/-/blob/90af0a65b33fc1e80d4f320ce61bc6fd04fe4831/core/tests/Drupal/Tests/Composer/Plugin/Scaffold/fixtures/drupal-core-fixture/composer.json#L2
        continue;
      }
      $subtrees[$project_name] = [
        'path' => $file->getRelativePath(),
        'description' => $json->description,
      ];
    }

    uksort($subtrees, [$this, 'comparePackageNames']);

    return $subtrees;
  }

  public function getVaultRepos($directory): array {
    $vault_repos = [];

    $finder = new Finder();
    $finder->directories()
      ->in($directory)
      ->depth(0);

    foreach ($finder as $file) {
      $vault_repos[] = $file->getFilename();
    }

    return $vault_repos;
  }

  /**
   * Sorts 'core-' and 'core' subtrees first.
   *
   * @param string $str1 <p>
   *   The first string
   * @param string $str2 <p>
   *   The second string
   *
   * @return int
   */
  public function comparePackageNames(string $str1, string $str2): int {
    return strcasecmp(
      $this->packageNameForComparison($str1),
      $this->packageNameForComparison($str2)
    );
  }

  /**
   * Prepends two spaces for 'core-' and one for 'core' itself.
   *
   * @param string $name
   *   Package name to compare.
   *
   * @return string
   *   Prefix to add before comparing.
   */
  public function packageNameForComparison(string $name): string {
    $name = trim($name);
    $space = ' ';

    if (strpos($name, 'core-') === 0) {
      return "{$space}{$space}{$name}";
    }

    if (strpos($name, 'core') === 0) {
      return "{$space}{$name}";
    }

    return $name;
  }

}
