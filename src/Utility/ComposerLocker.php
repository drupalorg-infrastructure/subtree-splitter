<?php

namespace DrupalCoreSplit\Utility;

use Composer\Json\JsonFile;
use Composer\Package\Locker;

/**
 * Manipulates Composer lock files.
 */
class ComposerLocker {

  /**
   * Update the Composer lock file in the given directory.
   *
   * @param string $directory
   *
   * @throws \Exception
   * @throws \Seld\JsonLint\ParsingException
   * @throws \UnexpectedValueException
   *
   * @see \Composer\Package\Locker::setLockData
   */
  public static function updateLockFile(string $directory): void {
    $json_file = new JsonFile($directory . '/composer.json');
    $json_file_contents = $json_file->read();
    $lock_file = new JsonFile($directory . '/composer.lock');
    $lock_file_contents = $lock_file->read();

    $content_hash = Locker::getContentHash(json_encode($json_file_contents));

    $lock_file_contents['content-hash'] = $content_hash;

    $lock_file->write($lock_file_contents);
  }

}
