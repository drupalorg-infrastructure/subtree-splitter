<?php

namespace DrupalCoreSplit\Utility;

use Github\Client;
use Github\ResultPager;

class GitHubApi {

  private $client;
  private $config;
  private $repos = [];

  public function __construct(ConfigHandler $config) {
    $this->client = new Client();
    $this->config = $config;
  }

  public function getRepos(): array {
    if ($this->repos) {
      return $this->repos;
    }
    $this->authenticate();
    $this->getRepoListViaApi();
    return $this->repos;
  }

  public function createRepo($name, $description) {
    $repo = $this->client->api('repo');
    $repo->create(
      $name,
      $description,
      $this->config->getDrupalOrgProjectUrl(),
      TRUE,
      $this->config->getGithubOrgName(),
      FALSE,
      FALSE,
      TRUE,
      $this->config->getGithubTeamId(),
      FALSE,
      FALSE
    );
  }

  private function authenticate() {
    $this->client->authenticate(
      $this->config->getGithubApiToken(),
      NULL,
      Client::AUTH_ACCESS_TOKEN
    );
  }

  private function getRepoListViaApi() {
    $paginator = new ResultPager($this->client);

    $results = $paginator->fetchAll(
      $this->client->api('user'),
      'repositories',
      [$this->config->getGithubOrgName()]
    );

    foreach ($results as $repo) {
      $this->repos[] = $repo['name'];
    }
  }

}
