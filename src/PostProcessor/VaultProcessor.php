<?php

namespace DrupalCoreSplit\PostProcessor;

use DrupalCoreSplit\Utility\ShellFacade;

class VaultProcessor implements PostProcessorInterface {

  protected $vault;
  private $shell;

  public function __construct($vault_dir) {
    $this->vault = $vault_dir;
    $this->shell = new ShellFacade();
  }

  /**
   * {@inheritdoc}
   */
  public function process(string $directory, string $ref, string $reftype, string $name) {
    // Push from $directory to $vault_dir.

    // Create destination if it doesn't exist.
    $vault_subtree = "{$this->vault}/${name}";
    if (!is_dir($vault_subtree)) {
      $this->shell->passthru("git init --bare {$vault_subtree}");
    }
    $this->shell->passthru("git -C {$directory} remote add vault {$vault_subtree}");
    // The --force is here because if we're changing the composer.json with
    // amended commits, the push will fail
    if ($reftype == 'tag') {
      $this->shell->passthru("git -C {$directory} tag -f {$ref}");
    }

    $this->shell->passthru("git -C {$directory} push --force vault $ref");

  }

}
