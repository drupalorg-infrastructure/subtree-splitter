<?php

namespace DrupalCoreSplit\Tests\Command;

use DrupalCoreSplit\Command\PurgeCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class PurgeCommandTest extends CommandTestCase {

  protected function createSut() {
    return new PurgeCommand();
  }

  public function testBasicConfiguration() {
    self::assertSame([
      'name' => 'purge',
      'arguments' => [
        'ref' => InputArgument::REQUIRED,
        'sha1' => InputArgument::REQUIRED,
        'reftype' => InputArgument::REQUIRED,
      ],
      'options' => [
        'config' => InputOption::VALUE_REQUIRED,
      ],
    ], $this->getBasicConfiguration());
  }

}
