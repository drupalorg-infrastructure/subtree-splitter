<?php

namespace DrupalCoreSplit\Tests\Command;

use DrupalCoreSplit\Command\PushCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class PushCommandTest extends CommandTestCase {

  protected function createSut() {
    return new PushCommand();
  }

  public function testBasicConfiguration() {
    self::assertSame([
      'name' => 'push',
      'arguments' => [
        'ref' => InputArgument::OPTIONAL,
        'sha1' => InputArgument::OPTIONAL,
        'reftype' => InputArgument::OPTIONAL,
      ],
      'options' => [
        'config' => InputOption::VALUE_REQUIRED,
        'no-packagist' => InputOption::VALUE_OPTIONAL,
        'push-all' => InputOption::VALUE_OPTIONAL,
      ],
    ], $this->getBasicConfiguration());
  }

}
