<?php

namespace DrupalCoreSplit\Tests\Command;

use DrupalCoreSplit\Command\PushPackagistCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class PushPackagistCommandTest extends CommandTestCase {

  protected function createSut() {
    return new PushPackagistCommand();
  }

  public function testBasicConfiguration() {
    self::assertSame([
      'name' => 'push-packagist',
      'arguments' => [
        'project-name' => InputArgument::REQUIRED,
        'ref' => InputArgument::OPTIONAL,
        'sha1' => InputArgument::OPTIONAL,
        'reftype' => InputArgument::OPTIONAL,
      ],
      'options' => [
        'config' => InputOption::VALUE_REQUIRED,
        'no-packagist' => InputOption::VALUE_OPTIONAL,
        'push-all' => InputOption::VALUE_OPTIONAL,
      ],
    ], $this->getBasicConfiguration());
  }

}
