<?php

namespace DrupalCoreSplit\Tests\Command;

use DrupalCoreSplit\Command\CreateLockCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class CreateLockCommandTest extends CommandTestCase {

  protected function createSut() {
    return new CreateLockCommand();
  }

  public function testBasicConfiguration() {
    self::assertSame([
      'name' => 'create-lock',
      'arguments' => [
        'ref' => InputArgument::REQUIRED,
        'sha1' => InputArgument::REQUIRED,
        'reftype' => InputArgument::REQUIRED,
      ],
      'options' => [
        'config' => InputOption::VALUE_REQUIRED,
      ],
    ], $this->getBasicConfiguration());
  }

}
