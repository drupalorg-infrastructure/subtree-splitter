<?php

namespace DrupalCoreSplit\Tests\Command;

use DrupalCoreSplit\Command\ConfigCommand;
use Symfony\Component\Console\Input\InputOption;

class ConfigCommandTest extends CommandTestCase {

  protected function createSut() {
    return new ConfigCommand();
  }

  public function testBasicConfiguration() {
    self::assertSame([
      'name' => 'config',
      'arguments' => [],
      'options' => [
        'config' => InputOption::VALUE_REQUIRED,
      ],
    ], $this->getBasicConfiguration());
  }

}
