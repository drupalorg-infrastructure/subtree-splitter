<?php

namespace DrupalCoreSplit\Tests\Command;

use DrupalCoreSplit\Command\SplitCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class SplitCommandTest extends CommandTestCase {

  protected function createSut() {
    return new SplitCommand();
  }

  public function testBasicConfiguration() {
    self::assertSame([
      'name' => 'split',
      'arguments' => [
        'ref' => InputArgument::REQUIRED,
        'sha1' => InputArgument::REQUIRED,
        'reftype' => InputArgument::REQUIRED,
      ],
      'options' => [
        'config' => InputOption::VALUE_REQUIRED,
        'split-all' => InputOption::VALUE_OPTIONAL,
      ],
    ], $this->getBasicConfiguration());
  }

}
