<?php

namespace DrupalCoreSplit\Tests\Command;

use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Tester\CommandTester;

abstract class CommandTestCase extends TestCase {

  use ProphecyTrait;

  protected function setUp(): void {
    $command = $this->createSut();
    $this->tester = new CommandTester($command);
  }

  abstract protected function createSut();

  protected function getBasicConfiguration(): array {
    $sut = $this->createSut();
    $definition = $sut->getDefinition();

    $config = ['name' => $sut->getName()];

    $config['arguments'] = array_map(static function (InputArgument $argument) {
      return $argument->isRequired() ? InputArgument::REQUIRED : InputArgument::OPTIONAL;
    }, $definition->getArguments());

    $config['options'] = array_map(static function (InputOption $option) {
      return $option->isValueRequired() ? InputOption::VALUE_REQUIRED : InputOption::VALUE_OPTIONAL;
    }, $definition->getOptions());

    return $config;
  }

}
