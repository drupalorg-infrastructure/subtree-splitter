<?php

namespace DrupalCoreSplit\Tests\Utility;

use DrupalCoreSplit\Utility\ComposerLocker;
use PHPUnit\Framework\TestCase;

class ComposerLockerTest extends TestCase {

  public function testUpdate() {
    // Arrange.
    $directory = sys_get_temp_dir();

    $composer_json = $directory . '/composer.json';
    $composer_json_contents = [
      'name' => 'test/example',
      'require' => [],
    ];
    $this->putFileContentsAsJson($composer_json, $composer_json_contents);

    $composer_lock = $directory . '/composer.lock';
    $composer_lock_contents = [
      // This happens to be the real content hash for the test composer.json
      // contents--though it doesn't actually matter for the purposes of this test.
      'content-hash' => '1d25614e835097e65e4090d4fd2f4fbb',
    ];
    $this->putFileContentsAsJson($composer_lock, $composer_lock_contents);

    // Act.
    $composer_json_contents['require']['lorem/ipsum'] = '*';
    $this->putFileContentsAsJson($composer_json, $composer_json_contents);
    ComposerLocker::updateLockFile($directory);

    // Assert.
    $this->getFileContentsFromJson($composer_lock)['content-hash'];
    // This is the real content hash obtained by actually running `composer install` on
    // the test composer.json. The operation is idempotent, so it can be depended upon.
    $expected_content_hash = 'e363a597d4475114776b72240f077182';
    $actual_content_hash = $this->getFileContentsFromJson($composer_lock)['content-hash'];
    self::assertSame($expected_content_hash, $actual_content_hash);

    // Cleanup.
    unlink($composer_json);
    unlink($composer_lock);
  }

  private function putFileContentsAsJson(string $json_file, array $composer_json_contents): void {
    file_put_contents($json_file, json_encode($composer_json_contents));
  }

  protected function getFileContentsFromJson(string $lock_file): array {
    return json_decode(file_get_contents($lock_file), TRUE);
  }

}
